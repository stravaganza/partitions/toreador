\version "2.19.82"

\include "header.ly"

\include "parts/Solo.ly"
inst = "Soliste"
linst = "Violoncelle Solo"
sinst = "Solo"
notes = \VlcSolo
outputName = "PDF/00-VlcSolo"
\include "piece.ly"

\include "parts/Flute.ly"
inst = "Flûte"
lints = "Grande Flûte"
sinst = "Flt."
notes = \Flute
outputName = "PDF/01-Flutes"
\include "piece.ly"

\include "parts/Piccolo.ly"
inst = "Pte Flûte"
lints = "Petite Flûte"
sinst = "Pic."
notes = \Piccolo
outputName = "PDF/02-Piccolo"
\include "piece.ly"

\include "parts/Hautbois.ly"
inst = "Hautbois"
lints = "Hautbois"
sinst = "Htbs."
notes = \Hautbois
outputName = "PDF/03-Hautbois"
\include "piece.ly"

\include "parts/Clarinette.ly"
inst = "Clarinettes"
linst = "Clarinettes en sib"
sinst = "Clar."
notes = \Clarinette
outputName = "PDF/04-Clarinettes"
\include "piece.ly"

\include "parts/ClarBasse.ly"
inst = "Clar Basse"
linst = "Clarinette Basse"
sinst = "B.Clar."
notes = \ClarBasse
outputName = "PDF/05-ClarBasse"
\include "piece.ly"

\include "parts/SaxAlto.ly"
inst = "Saxophone Alto"
linst = "Saxophone Alto"
sinst = "SaxA."
notes = \SaxAlto
outputName = "PDF/06-Saxophone-Alto"
\include "piece.ly"

\include "parts/SaxTenor.ly"
inst = "Saxophone Ténor"
lints = "Saxophone Ténor"
sinst = "SaxT."
notes = \SaxTenor
outputName = "PDF/07-SaxTenor"
\include "piece.ly"

\include "parts/Cor.ly"
inst = "Cor en Fa"
linst = "Cor en Fa"
sinst = "Cor"
notes = \Cor
outputName = "PDF/08-Cor"
\include "piece.ly"

\include "parts/Trompette.ly"
inst = "Trompette"
lints = "Trompette en sib"
sints = "Trmp."
notes = \Trompette
outputName = "PDF/09-Trompette"
\include "piece.ly"

\include "parts/Trombone.ly"
inst = "Trombone"
linst = "Trombone"
sinst = "Trmb."
notes = \Trombone
outputName = "PDF/9-Trombone"
\include "piece.ly"

\include "parts/Euphonium.ly"
inst = "Euphonium"
linst = "Euphonium en Sib"
sinst = "Euph."
notes = \Euphonium
outputName = "PDF/10-Euphonium"
\include "piece.ly"

\include "parts/Timbale.ly"
inst = "Timbale"
linst = "Timbale"
sinst = "Timb."
notes = \Timbale
outputName = "PDF/11-Timbale"
\include "piece.ly"

\include "parts/Triangle.ly"
inst = "Triangle"
linst = "Triangle"
sinst = "Trg."
notes = \Triangle
outputName = "PDF/12-Triangle"
\include "piece.ly"

\include "parts/Violons.ly"
inst = "Violons I"
linst = "Violons I"
sinst = "Vlns.I"
notes = \ViolonI
outputName = "PDF/13-ViolonsI"
\include "piece.ly"

\include "parts/Violon2.ly"
inst = "Violons II"
linst = "Violons II"
sinst = "Vlns.II"
notes = \ViolonII
outputName = "PDF/14-ViolonsII"
\include "piece.ly"

\include "parts/Alto.ly"
inst = "Altos"
linst = "Altos"
sinst = "Altos"
notes = \Alto
outputName = "PDF/15-Altos"
\include "piece.ly"

\include "parts/Violoncelle.ly"
inst = "Violoncelles"
lints = "Violoncelles"
sinst = "Vlcs."
notes = \Violoncelle
outputName = "PDF/16-Violoncelles"
\include "piece.ly"
