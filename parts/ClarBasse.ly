\version "2.19.82"

\include "header.ly"

ClarBasse = \relative c {
    \StandardConf
    \transposition bes,

    \clef "treble"
    \key g \minor
    \tempo 4 = 108

    \partial 4 r4

    R1

    \repeat volta 2 {
        \acciaccatura s8 R1*7

        \mark\default

        R1*8

        \mark\default

        R1*8

        \mark\default

        R1*11

        \mark\default

        \key f \major

        R1*10
        R1*6
        \bar "||"
        \mark\default

        R1*7
        \bar "||"
    }

    \alternative {
        {
            \key f \minor
            R1
        }
        {
            \key f \major
            R1
        }
    }

    R1*10
    \bar "|."

}


\new Score {
  \ClarBasse
}
