\version "2.19.82"

\include "header.ly"

VlcSolo = \relative c' {
    \StandardConf
    \clef bass \key f \minor
    \tempo 4 = 108

    \partial 4 r4
    R1 |

    \repeat volta 2 {

    \acciaccatura s8 R1*7 |

    \mark\default %A
    c8\f-\markup{\italic rude et très rythmé} b c4 r8 b8 c( bes) |
    \acciaccatura bes aes g aes4( ~8 g) aes16 r16 g8 |
    \acciaccatura g f e f4 ~16 r16 ees8 des c |
    des c bes4 ~8 r8 r4 |

    des'8\ff 4 8 c16( bes) aes4 r8 |
    bes8 4 8 aes16( g) f4 r8 |
    aes8\ff-\markup{\italic sempre} ees c( ees) aes bes c r16 des16 |
    ees4 \tuplet 3/2 {des8( c) bes} c4 ~8 r8 |

    \mark\default
    c8\f( b) c4 ~8 b c bes |
    \acciaccatura b aes g aes4( ~8 g) aes16 r16 g8 |
    \acciaccatura g f e f4( ~8 ees) d c |
    d c  bes4 ~8 r8 r4 |

    des'8\ff 4 8 c16( bes) aes4 r8 |
    bes8 4 8 aes16( g) f4 r8 |
    ges( des) bes des ges( aes) bes c |
    des4 \tuplet 3/2 {c8 b c} f4 ~8 r8\fermata |

    \mark\default
    c\mf b c4 ~8 g ees16 r16 f8 |
    \acciaccatura aes g fis g4( ~8 ees) c( d16) r16 |
    ees8( f) fis4 ~8 g\< aes g |
    ees'8.\f d16 c4 ~8 r8 r4 | 

    des8\f( c) des4 ~8 c ees des |
    \acciaccatura des c8. b16 c2 c,8 r8 |
    des' c des4 8 c ees8. des16 |
    c4 r8. c,16\cresc f4 r8. aes16 |
    g4 r8. c16 aes4 r8. c16\ff |
    e4 ~8 r8 c2\dim~ |
    1 |

    \mark\default
    \key f \major
    4\p-\markup{(légèrement et avec fatuité)} d8. c16 a8 ~16 r16 4 |
    \acciaccatura bes8 a8.( g16 a8. bes16) a4 ~8 r8 |
    bes4 g8. c16 a4 ~8 r8 |
    f4 d8. g16 c,4 ~8 r8 |
    g'2 8 d' c bes |
    \acciaccatura bes a g a bes a4 ~8 r8 |
    e4\< a a gis8 b |
    e1\cresc~ |
    8\! \tuplet 3/2 {d16[( e d)]} cis8 d g,\dim a bes4 |
    r8 \tuplet 3/2 {a16\p( bes a)} f8 d' c4 ~8 r8 |
    r8 \tuplet 3/2 {f,16\pp( g f)} c8 bes'-\markup{rit.} a4 \acciaccatura {g16 a} g4 |
    f-\markup{tempo} ~8 r8 r2 |
    R1*4 |

    \mark\default
    g2\pp-\markup{mais très marqué} 8 d' c bes |
    \acciaccatura bes a g a bes a4 ~8 r8 |
    e4 a a gis8 b |
    e1\cresc~
    8 \tuplet 3/2 {d16\f[( e d)]} cis8 d g, a bes4\dim |
    r8 \tuplet 3/2 {a16( bes a)} f8 d' c4 ~8 r8 |
    r8 \tuplet 3/2 {f,16\p( g f)} c8 bes' a8 r8 g8 r8 |
    }

    \alternative {
        {
            \key f \minor
            f8 r8 r4 r2 |
        }
        {
            \key f \major
            f8 r8 r4 r4 cis'8\p-\markup{\italic espress}( d) |
        }
    }
    e,8 ~16 r16 r4 r4 b'8( c) |
    d,( 16) r16 r4 r4 a'8( bes) |
    c,8 ~16 r16 f8.\f( a16) a8.( c16) c8.( f16) |
    8 r8 f,8.( a16) a8.( c16) c8.( d16) |
    2 ~8 r8 r4 |
    e4\ff c2\fermata ~8. f16 |
    <f f,>2 ~8 r8 r4 |
    R1*3 \bar "|."
}

% Temp

\new Score{
    \VlcSolo
}
