\version "2.19.82"

\include "header.ly"

Piccolo = \relative c'' {
    \transposition c'
    \StandardConf

    \clef "treble"
    \key f \minor
    \tempo 4 = 108

    \partial 4 \afterGrace c4-^\ff {d32( e f g aes bes)} |
    c8-.-^ b-.-^ c4-^ ~8 b-.-^ \acciaccatura des c-.-^ bes-.-^ |

    \repeat volta 2 {
        {\acciaccatura bes aes-.-^} g-.-^ aes4-^ ~8 g-.-^ \acciaccatura bes aes-.-^ g-.-^ |
        \acciaccatura g f-.-^ e-.-^ f4-^ ~8 ees-.-^ \acciaccatura ees des-.-^ c-.-^ |
        \acciaccatura ees des-.-^ c-.-^ bes4-^ ~8 \tuplet 3/2 {des16( ees f} \tuplet 3/2 {g aes bes} \tuplet 3/2 {c des ees} |
        des8-.) 4-^ 8-. c16-.[ bes-. aes8-.] ees-. aes-. |
        bes8-. 4-^ 8-. aes16-.[ g-. f8-.] c-. f-. |
        ges8-. \tuplet 3/2 {des16( c des} bes8-.) \tuplet 3/2 {des16( c des} ges8-.) \tuplet 3/2 {aes16( ges aes)} \tuplet 3/2 {bes( aes bes)} \tuplet 3/2 {c( bes c} |
        des8-.) r8 \tuplet 3/2 {c b c} f r8 r4 |

        \mark\default

        R1*8

        \mark\default

        R1*8

        \mark\default

        R1*11

        \mark\default

        \key f \major

        R1*10
        R1*6
        \bar "||"
        \mark\default

        R1*7
        \bar "||"
    }

    \alternative {
        {
            \key f \minor
            R1
        }
        {
            \key f \major
            R1
        }
    }

    R1*10
    \bar "|."

}

% Temp

\new Score{
    \Piccolo
}
