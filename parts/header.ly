\header {
    title = "Couplets du Toreador"
	subtitle = "Carmen"
	subsubtitle = "Acte II"
    composer = "Georges Bizet"
	arrangement = "Stravaganzé par Rémy H."
	tagline = "Stravaganza"
	date = "P20"
  }

StandardConf =
{
	\set Score.markFormatter = #format-mark-box-alphabet
	\compressFullBarRests
	\override MultiMeasureRest.expand-limit = #1
	\override BreathingSign.Y-offset = #3
	\override Staff.Rest.style = #'classical
}

