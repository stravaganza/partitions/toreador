\version "2.19.82"

\include "header.ly"

Flute = \relative c'' {
    \StandardConf
    \clef "treble"
    \key f \minor

    \tempo 4 = 108

    \partial 4 \afterGrace c4-^\ff {d32( e f g aes bes)} |
    c8-.-^ b-.-^ c4-^ ~8 b-.-^ \acciaccatura des c-.-^ bes-.-^ |

    \repeat volta 2 {
        {\acciaccatura bes aes-.-^} g-.-^ aes4-^ ~8 g'-.-^ \acciaccatura bes aes-.-^ g-.-^ |
        \acciaccatura g f-.-^ e-.-^ f4-^ ~8 ees-.-^ \acciaccatura ees des-.-^ c-.-^ |
        \acciaccatura ees des-.-^ c-.-^ bes4-^ ~8 \tuplet 3/2 {des,16( ees f} \tuplet 3/2 {g aes bes} \tuplet 3/2 {c des ees} |
        fes8-.) 4-^ 8-. ees16-.[ des-. c8-.] ees-. aes-. |
        des,-. 4-^ 8-. c16-.[ bes-. aes8-.] c-. f-. |
        bes,-. aes-. ges-. aes-. bes-. des-. ges-. aes-. |
        g-. r8 \tuplet 3/2 {c, b c} f r8 r4 |

        \mark\default

        R1*8

        \mark\default

        R1*8

        \mark\default

        R1*11

        \mark\default

        \key f \major

        R1*10
        R1*6
        \bar "||"
        \mark\default

        R1*7
        \bar "||"
    }

    \alternative {
        {
            \key f \minor
            R1
        }
        {
            \key f \major
            R1
        }
    }

    R1*10
    \bar "|."
}

% Temp

\new Score{
    \Flute
}
