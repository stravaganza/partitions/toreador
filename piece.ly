#(set-global-staff-size 20)
\book{
	\bookOutputName \outputName  
	\paper {
		line-width	= 180\mm
		left-margin   = 20\mm
		top-margin	= 10\mm
		bottom-margin = 20\mm
		ragged-last-bottom = ##f
		oddHeaderMarkup = \markup{
			\on-the-fly \not-first-page
			\fill-line{
				\fromproperty #'page:page-number-string
				\fromproperty #'header:instrument
				\fromproperty #'header:title
			}
		} 

		bookTitleMarkup = \markup {
			\column {
				\fill-line { \fromproperty #'header:dedication }
				\override #'(baseline-skip . 3.5)
				\column {
					\fill-line {
						\huge \larger \larger \bold
						\fromproperty #'header:title
					}
					\fill-line {
						\large \bold
						\fromproperty #'header:subtitle
						\fromproperty #'header:subsubtitle
					}
					\fill-line {
						\fromproperty #'header:instrument
						\fromproperty #'header:composer
						\fromproperty #'header:arrangement
					}
				}
			}
		}

		max-systems-per-page = 11
		evenHeaderMarkup = \oddHeaderMarkup
		markup-system-spacing = #'((basic-distance . 4) (padding . 3) (stretchability . 10))
	}

	\header {
		instrument = \inst
	}

	\score{ 
	\new PianoStaff
	<<
		\notes
	>>
	}

}
